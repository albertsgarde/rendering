#include <limits>
#include <algorithm>
#include "Torus.h"
#include "Quartic.h"

bool Torus::intersect(const optix::Ray& r, HitInfo& hit, unsigned int prim_idx) const
{
	const auto normalized_ray = normalize_ray(r);

	const auto t_bounds = this->t_bounds(normalized_ray);

	if (!t_bounds)
		return false;

	const auto quartic = intersection_quartic(normalized_ray);
	const auto derivative = quartic.derivative();

	auto maxima = derivative.find_roots();
	std::sort(maxima.begin(), maxima.end());

	// Find the first interval with a root.
	auto t_min = t_bounds.value().first;
	auto t_min_value = quartic.evaluate(t_min);
	if (t_min_value == 0) // The lower bound is a root.
		return return_intersection_for_root(r, normalized_ray, t_min, hit);
	for (auto maximum : maxima)
	{
		if (maximum < t_min)  // The maximum is less than the lower bound. Discard.
			continue;
		else
		{
			auto value_at_maximum = quartic.evaluate(maximum);
			if (value_at_maximum == 0) // The current maximum is a root.
				return return_intersection_for_root(r, normalized_ray, maximum, hit);
			if (t_min_value > 0 != value_at_maximum > 0) // The points are on either side of zero in the quartic. There must be a root.
				return return_intersection_for_root(r, normalized_ray, quartic.find_root(t_min, maximum), hit);
			else // Set the current maximum as the new t_min.
			{
				t_min = maximum;
				t_min_value = value_at_maximum;
			}
		}
	}
	// There are no roots between the lower bound an the largest root.
	// Check between the largest root and the upper bound.
	const auto t_max = t_bounds.value().second;
	const auto t_max_value = quartic.evaluate(t_max);
	if (t_max_value == 0) // The upper bound is a root. 
		return return_intersection_for_root(r, normalized_ray, t_max, hit);
	if (t_min_value > 0 != t_max_value > 0) // The points are on either side of zero in the quartic. There must be a root.
		return return_intersection_for_root(r, normalized_ray, quartic.find_root(t_min, t_max), hit);
	// There are no roots between the bounds.
	// Therefore no intersections exist.
	return false;

}

void Torus::transform(const optix::Matrix4x4 &matrix)
{
	throw("Damn. I was kinda hoping this wouldn't be used...");
}

optix::Aabb Torus::compute_bbox() const
{
	const auto orth1 = optix::make_float3(normalization_matrix.getCol(0));
	const auto orth2 = optix::make_float3(normalization_matrix.getCol(2));
	
	const auto k_x = sqrt(orth1.x * orth1.x + orth2.x * orth2.x);
	const auto k_y = sqrt(orth1.y * orth1.y + orth2.y * orth2.y);
	const auto k_z = sqrt(orth1.z * orth1.z + orth2.z * orth2.z);

	const auto k = optix::make_float3(k_x + minor_radius, k_y + minor_radius, k_z + minor_radius);
	const auto min = center - k;
	const auto max = center + k;
	return optix::Aabb(min, max);
}

optix::Matrix4x4 Torus::find_normalization_matrix_inverse(optix::float3 centre, optix::float3 normal, float major_radius)
{
	constexpr float epsilon = 1e-6;
	// If the x coordinate isn't too small, the cross product with (0,0,1) will work, otherwise the cross product with (1,0,0) will.
	const auto normalized_normal = normalize(normal);
	const auto orth1 = normalize(abs(normal.x) > epsilon ? optix::make_float3(-normal.y, normal.x, 0) : optix::make_float3(0, -normal.z, normal.y));
	const auto orth2 = normalize(optix::cross(normal, orth1));

	// Construct a transformation from the normalized torus coordinates to the world coordinates.
	// Orthonormal basis matrix.
	auto rotation_matrix = optix::Matrix3x3();
	rotation_matrix.setCol(0, orth1);
	rotation_matrix.setCol(1, normalized_normal);
	rotation_matrix.setCol(2, orth2);
	rotation_matrix *= major_radius; // Scale with the major radius.

	auto result = optix::Matrix4x4::translate(centre); // Translate so origin is at center.
	for (auto i = 0; i < 3; ++i) // Copy the rotation/scale matrix into the 4x4 matrix.
		result.setCol(i, optix::make_float4(rotation_matrix.getCol(i), 0));

	// Return the inverse.
	return result;
}

optix::Ray Torus::normalize_ray(const optix::Ray& ray) const
{
	const auto norm_origin = normalization_matrix * optix::make_float4(ray.origin, 1); // 1 in last coord because translations is wanted.
	const auto norm_dir = normalization_matrix * optix::make_float4(ray.direction, 0); // 0 in last coord because translations is not wanted.
	return optix::Ray(optix::make_float3(norm_origin), optix::make_float3(norm_dir), ray.ray_type, ray.tmin, ray.tmax);
}

Quartic Torus::intersection_quartic(const optix::Ray& normalized_ray) const
{
	// Variables are made to match the formula in the report.

	const auto omega = normalized_ray.direction;
	const auto o = normalized_ray.origin;
	const auto R = normalized_torus.minor_radius();

	const auto omega_omega = dot(omega, omega);
	const auto oo = dot(o, o);
	const auto o_omega = dot(o, omega);
	const auto RR = R * R;
	const auto oo_minus_RR_minus_1 = oo - RR - 1;

	const auto a = omega_omega * omega_omega;
	const auto b = 4 * o_omega * omega_omega;
	const auto c = 4 * o_omega * o_omega + 2 * oo_minus_RR_minus_1 * omega_omega + 4 * omega.y * omega.y;
	const auto d = 4 * oo_minus_RR_minus_1 * o_omega + 8 * o.y * omega.y;
	const auto e = (oo - 2 * (RR + 1)) * oo + 4 * o.y * o.y + 1;

	return Quartic(a, b, c, d, e);
}

std::optional<std::pair<float, float>> Torus::t_bounds(const optix::Ray& normalized_ray) const
{
	// Strongly inspired by https://tavianator.com/2011/ray_box.html.
	static_assert(std::numeric_limits<double>::is_iec559, "Please use IEEE754, you weirdo");
	const auto origin = normalized_ray.origin;
	const auto inv_dir = optix::make_float3(1/normalized_ray.direction.x, 1 / normalized_ray.direction.y, 1 / normalized_ray.direction.z);
	const auto xz_max = 1 + normalized_torus.minor_radius();
	const auto xz_min = -xz_max;
	const auto y_max = normalized_torus.minor_radius();
	const auto y_min = -y_max;
	
	const auto tx1 = (xz_max - origin.x) * inv_dir.x;
	const auto tx2 = (xz_min - origin.x) * inv_dir.x;
	const auto tx = std::minmax(tx1, tx2);

	const auto tz1 = (xz_max - origin.z) * inv_dir.z;
	const auto tz2 = (xz_min - origin.z) * inv_dir.z;
	const auto tz = std::minmax(tz1, tz2);

	const auto ty1 = (y_max - origin.y) * inv_dir.y;
	const auto ty2 = (y_min - origin.y) * inv_dir.y;
	const auto ty = std::minmax(ty1, ty2);
	
	const auto result = std::pair(std::max({ tx.first, ty.first, tz.first, normalized_ray.tmin}), std::min({ tx.second, ty.second, tz.second, normalized_ray.tmax }));
	return result.first <= result.second ? std::optional(result) : std::nullopt;
}

bool Torus::return_intersection_for_root(const optix::Ray& ray, const optix::Ray& normalized_ray, float root, HitInfo& hit) const
{
	const auto normalized_position = normalized_ray.origin + normalized_ray.direction * root;
	// The point on the major circle neares the intersection point.
	const auto nearest_major_circle_point = normalize(optix::make_float3(normalized_position.x, 0, normalized_position.z));
	const auto normalized_geometric_normal = normalized_position - nearest_major_circle_point;

	hit.has_hit = true;
	hit.dist = root;
	hit.position = ray.origin + ray.direction * root;
	hit.geometric_normal = normalize(optix::make_float3(normalization_matrix_inverse * optix::make_float4(normalized_geometric_normal, 0)));
	hit.shading_normal = hit.geometric_normal;
	hit.material = &material;
	hit.texcoord = hit.geometric_normal;
	return true;
}

// 02562 Rendering Framework
// Written by Jeppe Revall Frisvad, 2011
// Copyright (c) DTU Informatics 2011

#include <optix_world.h>
#include "HitInfo.h"
#include "Sphere.h"

using namespace optix;

bool Sphere::intersect(const Ray& r, HitInfo& hit, unsigned int prim_idx) const
{
	// Vector from ray origin to sphere center.
	const auto origin_to_center = center - r.origin;
	// Distance from the origin of the ray to the point closest to the center of the circle.
	const auto dist_to_closest_point = dot(origin_to_center, r.direction);

	// Square of distance from point mentioned above to possible intersections.
	const auto square_dist_closest_point_to_edge = radius * radius + dist_to_closest_point * dist_to_closest_point - dot(origin_to_center, origin_to_center);
	if (square_dist_closest_point_to_edge < 0)
		// The square of the distance is only negative if the point is outside the sphere.
		return false;
	// We know this is positive from the test above.
	const auto dist_closest_point_to_edge = sqrt(square_dist_closest_point_to_edge);
	auto t = dist_to_closest_point - dist_closest_point_to_edge;
	if (t < r.tmin || t > r.tmax)
	{
		t = dist_to_closest_point + dist_closest_point_to_edge;
		if (t < r.tmin || t > r.tmax)
			return false;
	}
	hit.has_hit = true;
	hit.dist = t;
	hit.position = r.origin + r.direction * t;
	hit.geometric_normal = normalize(hit.position - center);
	hit.shading_normal = hit.geometric_normal;
	hit.material = &material;
	hit.texcoord = hit.geometric_normal;
	return true;
  // Implement ray-sphere intersection here.
  //
  // Input:  r                    (the ray to be checked for intersection)
  //         prim_idx             (index of the primitive element in a collection, not used here)
  //
  // Output: hit.has_hit          (set true if the ray intersects the sphere)
  //         hit.dist             (distance from the ray origin to the intersection point)
  //         hit.position         (coordinates of the intersection point)
  //         hit.geometric_normal (the normalized normal of the sphere)
  //         hit.shading_normal   (the normalized normal of the sphere)
  //         hit.material         (pointer to the material of the sphere)
  //        (hit.texcoord)        (texture coordinates of intersection point, not needed for Week 1)
  //
  // Return: True if the ray intersects the sphere, false otherwise
  //
  // Relevant data fields that are available (see Sphere.h and OptiX math library reference)
  // r.origin                     (ray origin)
  // r.direction                  (ray direction)
  // r.tmin                       (minimum intersection distance allowed)
  // r.tmax                       (maximum intersection distance allowed)
  // center                       (sphere center)
  // radius                       (sphere radius)
  // material                     (material of the sphere)
  //
  // Hints: (a) The square root function is called sqrt(x).
  //        (b) There is no need to handle the case where the 
  //            discriminant is zero separately.

}

void Sphere::transform(const Matrix4x4& m)
{
  float3 radius_vec = make_float3(radius, 0.0f, 0.0f) + center;
  radius_vec = make_float3(m*make_float4(radius_vec, 1.0f));
  center = make_float3(m*make_float4(center, 1.0f)); 
  // The radius is scaled by the X scaling factor.
  // Not ideal, but the best we can do without elipsoids
  radius_vec -= center;
  radius = length(radius_vec);  
}

Aabb Sphere::compute_bbox() const
{
  Aabb bbox;
  bbox.include(center - make_float3(radius, 0.0f, 0.0f));
  bbox.include(center + make_float3(radius, 0.0f, 0.0f));
  bbox.include(center - make_float3(0.0f, radius, 0.0f));
  bbox.include(center + make_float3(0.0f, radius, 0.0f));
  bbox.include(center - make_float3(0.0f, 0.0f, radius));
  bbox.include(center + make_float3(0.0f, 0.0f, radius));
  return bbox;
}

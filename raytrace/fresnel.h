// 02562 Rendering Framework
// Written by Jeppe Revall Frisvad, 2011
// Copyright (c) DTU Informatics 2011

#ifndef FRESNEL_H
#define FRESNEL_H


inline float fresnel_r_s(float cos_theta1, float cos_theta2, float ior1, float ior2)
{
  // Compute the perpendicularly polarized component of the Fresnel reflectance
	auto denominator = (ior1 * cos_theta1 + ior2 * cos_theta2);
	if (denominator == 0) {
		std::cout << "Trying to devide by 0.";
	}
	return (ior1 * cos_theta1 - ior2 * cos_theta2) / denominator;
}

inline float fresnel_r_p(float cos_theta1, float cos_theta2, float ior1, float ior2)
{
  // Compute the parallelly polarized component of the Fresnel reflectance
	auto denominator = (ior2 * cos_theta1 + ior1 * cos_theta2);
	if (denominator == 0) {
		std::cout << "Trying to devide by 0.";
	}
	return (ior2 * cos_theta1 - ior1 * cos_theta2) / denominator;
}

/// <summary>
/// Calculate the Fresnel reflectance R.
/// </summary>
/// <param name="cos_theta1">
/// cosinus to tetha_i on slides (The angle of incidence).
/// </param>
/// <param name="cos_theta2">
/// cosinus to tetha_t on slides (The angle of refraction).
/// </param>
/// <param name="ior1">
/// n_i on slides (the refractive index of the medium from which the incident ray reaches the surfac).
/// </param>
/// <param name="ior2">
/// n_t on slides ( the refractive index of the medium that the ray transmits into).
/// </param>
/// <returns> returns R as a float </returns>
inline float fresnel_R(float cos_theta1, float cos_theta2, float ior1, float ior2)
{
  // Compute the Fresnel reflectance using fresnel_r_s(...) and fresnel_r_p(...)
	auto frensel_r_s_var = fresnel_r_s(cos_theta1, cos_theta2, ior1, ior2);
	auto frensel_r_p_var = fresnel_r_p(cos_theta1, cos_theta2, ior1, ior2);
  return 1./2. * (frensel_r_s_var * frensel_r_s_var + frensel_r_p_var * frensel_r_p_var);
}

#endif // FRESNEL_H

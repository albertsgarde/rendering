// 02562 Rendering Framework
// Written by Jeppe Revall Frisvad, 2011
// Copyright (c) DTU Informatics 2011

#include <iostream>
#include <optix_world.h>
#include "mt_random.h"
#include "Shader.h"
#include "HitInfo.h"
#include "RayCaster.h"

using namespace std;
using namespace optix;

float3 RayCaster::compute_pixel(unsigned int x, unsigned int y) const
{
  float3 result = make_float3(0.0f);

  // Go through all subpixels' jitter point and shade the ray through that point.
  // Sum the results.
  for (unsigned int i = 0; i < subdivs; ++i)
  {
      for (unsigned int j = 0; j < subdivs; ++j)
      {
          auto ray = scene->get_camera()->get_ray(lower_left + 
              make_float2(x * win_to_ip.x, y * win_to_ip.y) + jitter[i * subdivs + j]);

          HitInfo hit = HitInfo();

          if (scene->closest_hit(ray, hit))
              result += get_shader(hit)->shade(ray, hit);
          else
              result += get_background(ray.direction);
      }
  }

  // Divide the result by the number of sub pixels to get the average.
  return result * one_over_num_subpixels;
}

float3 RayCaster::get_background(const float3& dir) const
{ 
  if(!sphere_tex)
    return background;
  return make_float3(sphere_tex->sample_linear(dir));
}

void RayCaster::increment_pixel_subdivs()
{
  ++subdivs;
  compute_jitters();
  cout << "Rays per pixel: " << subdivs*subdivs << endl;
}

void RayCaster::decrement_pixel_subdivs()
{
  if(subdivs > 1)
  {
    --subdivs;
    compute_jitters();
  }
  cout << "Rays per pixel: " << subdivs*subdivs << endl;
}

void RayCaster::compute_jitters()
{
  // Used in compute_pixel.
  one_over_num_subpixels = 1.0f / (subdivs * subdivs);

  float aspect = width/static_cast<float>(height);
  win_to_ip.x = win_to_ip.y = 1.0f/static_cast<float>(height);
  lower_left = (win_to_ip - make_float2(aspect, 1.0f))*0.5f;
  step = win_to_ip/static_cast<float>(subdivs);

  jitter.resize(subdivs*subdivs);
  for(unsigned int i = 0; i < subdivs; ++i)
    for(unsigned int j = 0; j < subdivs; ++j)
      jitter[i*subdivs + j] = make_float2(safe_mt_random() + j, safe_mt_random() + i)*step - win_to_ip*0.5f; 
}

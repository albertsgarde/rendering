// 02576 Rendering Framework
// Written by Jeppe Revall Frisvad, 2010
// Copyright (c) DTU Informatics 2010

#include <optix_world.h>
#include "HitInfo.h"
#include "Volume.h"

using namespace optix;

float3 Volume::shade(const Ray& r, HitInfo& hit, bool emit) const
{
  // If inside the volume, Find the direct transmission through the volume by using
  // the transmittance to modify the result from the Transparent shader.
    if (1 < hit.ray_ior)
    {
        const auto T = get_transmittance(hit);
        const auto L0 = Transparent::shade(r, hit, emit);
        return make_float3(L0.x * T.x, L0.y * T.y ,L0.z * T.z);
    }
    else 
        return Transparent::shade(r, hit, emit);
}

float3 Volume::get_transmittance(const HitInfo& hit) const
{
  if(hit.material)
  {
    // Compute and return the transmittance using the diffuse reflectance of the material.
    // Diffuse reflectance rho_d does not make sense for a specular material, so we can use 
    // this material property as an absorption coefficient. Since absorption has an effect
    // opposite that of reflection, using 1/rho_d-1 makes it more intuitive for the user.
    const float3 rho_d = make_float3(hit.material->diffuse[0], hit.material->diffuse[1], hit.material->diffuse[2]);
    const auto s = hit.dist;
    auto T = make_float3(0.0f);
    if (rho_d.x != 0)
    {
        T.x = std::exp(-(1 / rho_d.x - 1) * s);
    }
    if (rho_d.y != 0)
    {
        T.y = std::exp(-(1 / rho_d.y - 1) * s);
    }
    if (rho_d.z != 0)
    {
        T.z = std::exp(-(1 / rho_d.z - 1) * s);
    }
    return T;
  }
  return make_float3(1.0f);
}



// 02562 Rendering Framework
// Written by Jeppe Revall Frisvad, 2011
// Copyright (c) DTU Informatics 2011

#include <optix_world.h>
#include "IndexedFaceSet.h"
#include "ObjMaterial.h"
#include "mt_random.h"
#include "cdf_bsearch.h"
#include "HitInfo.h"
#include "AreaLight.h"

using namespace optix;

/// <summary>
/// Compute the irradiance from this light source at a given point.
/// </summary>
/// <param name="pos">The position for which to find the irradiance.</param>
/// <param name="dir">Output argument for the direction to the light.</param>
/// <param name="L">Output argument for the calculated irradiance.</param>
/// <returns>True if shadows are off or the point is not blocked.</returns>
bool AreaLight::sample(const float3& pos, float3& dir, float3& L) const
{
	const IndexedFaceSet& const normals = mesh->normals;
	const auto& const geometry = mesh->geometry;

	
	auto triangle = static_cast<int>(floorf(mt_random_half_open()*mesh->get_no_of_primitives()));
	while (triangle >= mesh->get_no_of_primitives()) // Very rarely, the triangle index will be out of bounds.
		triangle = static_cast<int>(floorf(mt_random_half_open() * mesh->get_no_of_primitives()));
	const auto& const face = geometry.face(triangle);
	const auto rand1_sqrt = sqrt(mt_random());
	const auto rand2 = mt_random();
	// Taken from StackOverflow question here: https://math.stackexchange.com/questions/18686/uniform-random-point-in-triangle.
	const auto x_coef = 1 - rand1_sqrt;
	const auto y_coef = rand1_sqrt * (1 - rand2);
	const auto z_coef = rand1_sqrt * rand2;
	
	// Normal at point.
	const auto light_pos = (geometry.vertex(face.x) * x_coef + geometry.vertex(face.y) * y_coef + geometry.vertex(face.z) * z_coef);
	const auto normal = (normals.vertex(face.x) * x_coef + normals.vertex(face.y) * y_coef + normals.vertex(face.z) * z_coef);

	const auto pos_to_light = light_pos - pos;
	dir = normalize(pos_to_light);


	const auto intensity = mesh->face_areas.at(triangle) * get_emission(triangle) * dot(normalize(normal), -dir);

	// Apply the inverse square law to get the irradiance.
	L = intensity / dot(pos_to_light, pos_to_light);

	// Check whether there are any objects in the way of the beam if shadows are on.
	if (shadows)
	{
		auto ray = Ray(pos, dir, 0, 1e-4, length(pos_to_light) * (1 - 1e-4));
		HitInfo hit;
		// If an object is in the way, return false.
		if (tracer->trace_to_any(ray, hit))
			return false;
	}
	return true;
}

bool AreaLight::emit(Ray& r, HitInfo& hit, float3& Phi) const
{
	// Generate and trace a ray carrying radiance emitted from this area light.
	//
	// Output: r    (the new ray)
	//         hit  (info about the ray-surface intersection)
	//         Phi  (the flux carried by the emitted ray)
	//
	// Return: true if the ray hits anything when traced
	//
	// Relevant data fields that are available (see Light.h and Ray.h):
	// tracer              (pointer to ray tracer)
	// geometry            (indexed face set of triangle vertices)
	// normals             (indexed face set of vertex normals)
	// no_of_faces         (number of faces in triangle mesh for light source)
	// mesh->surface_area  (total surface area of the light source)
	// r.origin            (starting position of ray)
	// r.direction         (direction of ray)

	// Get geometry info
	const IndexedFaceSet& geometry = mesh->geometry;
	const IndexedFaceSet& normals = mesh->normals;
	const float no_of_faces = static_cast<float>(geometry.no_faces());

	// Sample ray origin and direction

	// Trace ray

	// If a surface was hit, compute Phi and return true

	return false;
}

float3 AreaLight::get_emission(unsigned int triangle_id) const
{
	const ObjMaterial& mat = mesh->materials[mesh->mat_idx.at(triangle_id)];
	return make_float3(mat.ambient[0], mat.ambient[1], mat.ambient[2]);
}

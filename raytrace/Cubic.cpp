#include<cmath>
#include "Cubic.h"
#include <optix_world.h>

std::vector<float> Cubic::find_roots() const
{
	// p and q in the depressed cubic t^3+pt+q.
	const double double_a = a;
	const double double_b = b;
	const double double_c = c;
	const double double_d = d;
	const auto aa = double_a * double_a;
	const auto bb = double_b * double_b;
	const auto ac = double_a * double_c;
	const auto p = (3 * ac - bb) / (3 * aa);
	const auto q = (2 * bb * double_b - 9 * ac * double_b + 27 * aa * double_d) / (27 * double_a * aa);
	const auto ppp = p * p * p;
	const auto qq = q * q;
	const auto discriminant = -(4 * ppp + 27 * qq);

	const auto b_over_3a = double_b / (3 * double_a);
	
	// If the discriminant is non-positive, assume single real root and use Cardano's formula.
	if (discriminant <= 0)
	{
		const auto minus_half_q = -q * 0.5;
		// The square root used twice in Cardano's formula.
		const auto inner_sqrt = sqrt(qq * 0.25 + ppp / 27.);
		const auto root = std::cbrt(minus_half_q + inner_sqrt) + std::cbrt(minus_half_q - inner_sqrt);
		// Transform the root back and return.
		return std::vector<float>(1, root-b_over_3a);
	}
	// If the discriminant is positive, use Vi�te's trigonometric solution.
	auto result = std::vector<float>(3, 0);
	const auto one_over_p = 1. / p;
	const auto sqrt_minus_4p_over_3 = sqrt(-4. * p / 3.);
	const auto arccos_term = acos(1.5 * q * one_over_p * sqrt(-3 * one_over_p))/3.;
	constexpr auto two_thirds_pi = M_PIf / 1.5;
	for (int i = 0; i < 3; ++i)
		// The final term is to convert back to non-depressed cubic.
		result.at(i) = sqrt_minus_4p_over_3 * cos(arccos_term - two_thirds_pi * i)-b_over_3a;
	return result;

	// This function sometimes has large errors. Roots have differed from Maple's results by up to 10^-3.
	// For now we assume this is fine.
	// A possible solution would be to do a single step of Newton-Raphson at the end, but this risks rare catastrophic results.
}

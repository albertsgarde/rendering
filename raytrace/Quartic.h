#pragma once
#include "Cubic.h"

class Quartic
{
public:
	Quartic(float a, float b, float c, float d, float e) noexcept
		: a(a), b(b), c(c), d(d), e(e)
	{ }

	inline float evaluate(float x) const noexcept
	{
		return e + x * (d + x * (c + x * (b + x * a)));
	}

	/// <summary>
	/// Finds the only root in the interval.
	/// The interval given must bracket a root, and the result is undefined if there are multiple roots.
	/// Uses bisection and Newton-Raphson roughly as in Numerical Recipes.
	/// </summary>
	/// <param name="min_x">The lower bound of the bracket.</param>
	/// <param name="max_x">The upper bound of the bracket.</param>
	/// <returns>The only root within the interval.</returns>
	float find_root(float min_x, float max_x) const;

	Cubic derivative() const noexcept
	{
		return Cubic(a * 4, b * 3, c * 2, d);
	}

private:
	/// <summary>
	/// Coefficients for the polynomial.
	/// P(x)=ax^4+bx^3+cx^2+dx+e
	/// </summary>
	const float a, b, c, d, e;
};

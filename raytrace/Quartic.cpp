#include <cassert>
#include <algorithm>
#include <iostream>
#include "Quartic.h"

float Quartic::find_root(const float min_x, const float max_x) const
{
	constexpr int maximum_iterations = 100;

	const auto derivative = Quartic::derivative();

	const auto min_x_value = evaluate(min_x);
	const auto max_x_value = evaluate(max_x);
	
	if (min_x_value == 0)
		return min_x;
	if (max_x_value == 0)
		return max_x;

	if (min_x_value > 0 && max_x_value > 0 || min_x_value < 0 && max_x_value < 0)
		throw("Root must be bracketed by min_x and max_x");
	assert((min_x_value > 0) != (max_x_value > 0));

	// The current bound with lowest function value.
	auto x_low = min_x_value < 0 ? min_x : max_x;
	// The current bound with highest function value.
	auto x_high = min_x_value < 0 ? max_x : min_x;

	auto interval_size = abs(x_high-x_low);
	// Current guess for the root.
	auto guess = 0.5f * (x_low + x_high);
	// The step size before the last.
	auto dx_old = interval_size;
	// The last step size.
	auto dx = dx_old;
	// The function value at the current root guess.
	auto f = evaluate(guess);
	// The derivative at the current root guess.
	auto df = derivative.evaluate(guess);

	for (auto i = 0; i < maximum_iterations; ++i)
	{
		if ((((guess - x_high) * df - f) * ((guess - x_low) * df - f) > 0.0f) // Bisect if Newton out of range,
			|| (abs(2.0f * f) > abs(dx_old * df))) // or not decreasing fast enough.
		{
			dx_old = dx;
			dx = 0.5f * (x_high - x_low);
			guess = x_low + dx;
			if (guess == x_low) // Change is invisible. Return.
				return guess;
		}
		else // Newton is acceptable. Take step.
		{
			dx_old = dx;
			dx = f / df;
			if (isnan(dx))
			{
				std::cout << "used hack" << std::endl;
				return guess;//bit of a hack.
			}
			const auto temp = guess;
			guess -= dx;
			if (guess == temp) // Change is invisible. Return.
				return guess;
		}
		f = evaluate(guess); // Update function value to new guess
		df = derivative.evaluate(guess); // Same for derivative.
		if (f < 0.0) // Maintain the bracket on the root.
			x_low = guess;
		else
			x_high = guess;
		if (abs(x_high - x_low) >= interval_size) // The interval size isn't getting any smaller.
			return guess;
		interval_size = abs(x_high - x_low);
	}
	throw("Maximum number of iterations exceeded in rtsafe");
}

#pragma once
#include <optix_world.h>
#include <optional>
#include "HitInfo.h"
#include "ObjMaterial.h"
#include "Object3D.h"
#include "Quartic.h"

class Torus : public Object3D
{
public:
	Torus(optix::float3 centre, optix::float3 normal, float major_radius, float minor_radius, const ObjMaterial& material)
		: center(centre), normal(normal), major_radius(major_radius), minor_radius(minor_radius),
		normalized_torus(minor_radius / major_radius), normalization_matrix_inverse(find_normalization_matrix_inverse(centre, normal, major_radius)),
		normalization_matrix(normalization_matrix_inverse.inverse()), material(material)
	{}

	bool intersect(const optix::Ray& r, HitInfo& hit, unsigned int prim_idx) const override;
	void transform(const optix::Matrix4x4& m) override;
	optix::Aabb compute_bbox() const override;
	void compute_bsphere(optix::float3& center, float& radius) const noexcept override
	{
		center = this->center;
		radius = major_radius + minor_radius;
	}

	const optix::float3& get_center() const { return center; }
	const optix::float3& get_normal() const { return normal; }
	float get_major_radius() const { return major_radius; }
	float get_minor_radius() const { return minor_radius; }
	const ObjMaterial& get_material() const { return material; }

private:
	class NormalizedTorus
	{
	public:
		NormalizedTorus(float minor_radius) noexcept
			: m_minor_radius(minor_radius)
		{ }

		inline float minor_radius() const noexcept
		{
			return m_minor_radius;
		}
	private:
		const float m_minor_radius;
	};

	/// <summary>
	/// Transforms the ray to the same coordinate system as the normalized torus.
	/// </summary>
	/// <param name="r">The ray to normalize.</param>
	/// <returns>The given ray transformed to the normalized torus' coordinates.</returns>
	optix::Ray normalize_ray(const optix::Ray& r) const;

	static optix::Matrix4x4 find_normalization_matrix_inverse(optix::float3 centre, optix::float3 normal, float major_radius);

	/// <summary>
	/// Finds a fourth degree polynomial with roots exactly when the normalized ray intersects the normalized torus.
	/// Also, the polynomial will be positive iff the ray is outside the torus.
	/// </summary>
	/// <param name="normalized_ray">A ray normalized with this torus' normalize_ray function.</param>
	/// <returns>A fourth degree polynomial with roots exactly when the normalized ray intersects the normalized torus.</returns>
	Quartic intersection_quartic(const optix::Ray& normalized_ray) const;

	/// <summary>
	/// Finds if and where the normalized ray enters and exits an AABB of the normalized torus.
	/// Takes minimum and maximum values of ray into consideration.
	/// </summary>
	/// <param name="normalized_ray">A ray normalized with this torus' normalize_ray function.</param>
	/// <returns>An optional with a pair of (t_min, t_max) if the ray hits the AABB and a std::nullopt if not.</returns>
	std::optional<std::pair<float, float>> t_bounds(const optix::Ray& normalized_ray) const;

	bool return_intersection_for_root(const optix::Ray& ray, const optix::Ray& normalized_ray, float root, HitInfo& hit) const;

	const optix::float3 center;
	const optix::float3 normal;
	const float major_radius;
	const float minor_radius;

	const NormalizedTorus normalized_torus;
	const optix::Matrix4x4 normalization_matrix_inverse;
	const optix::Matrix4x4 normalization_matrix;

	ObjMaterial material;
};

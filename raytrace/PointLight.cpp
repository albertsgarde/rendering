// 02562 Rendering Framework
// Written by Jeppe Revall Frisvad, 2011
// Copyright (c) DTU Informatics 2011

#include <optix_world.h>
#include "HitInfo.h"
#include "mt_random.h"
#include "PointLight.h"

using namespace optix;

/*
	Casts ray from position to light to find radiance.
*/
bool PointLight::sample(const float3& pos, float3& dir, float3& L) const
{	
	const auto pos_to_light = light_pos - pos;
	// Create directional vector towards the light.
	dir = normalize(pos_to_light);
	// Adjust the intensity using the inverse square law.
	L = intensity / dot(pos_to_light, pos_to_light);
	// If shadows are turned on, test to see if light can reach the sample point.
	if (shadows)
	{
		auto ray = Ray(pos, dir, 0, 1e-4, length(pos_to_light)*(1- 1e-4));
		HitInfo hit;
		return !tracer->trace_to_any(ray, hit);
	}
	return true;
}

/// <summary>
/// Traces a ray in a random direction.
/// </summary>
/// <param name="r">An output argument for the ray being traced.</param>
/// <param name="hit">An output argument for the resulting HitInfo.</param>
/// <param name="Phi">An output argument for the intensity of the light at 
/// the hit location.</param>
/// <returns>Whether the ray hits anything.</returns>
bool PointLight::emit(Ray& r, HitInfo& hit, float3& Phi) const
{
	// Use von Neumann rejection sampling to find a random direction.
	float3 direction;
	do
		direction = make_float3(mt_random() * 2 - 1, 
			mt_random() * 2 - 1, mt_random() * 2 - 1);
	while (dot(direction, direction) > 1);

	// Create a ray in the random direction.
	r = Ray(light_pos, normalize(direction), 0, 0);

	// If the ray hits anything, calculate radiant flux.
	if (tracer->trace_to_closest(r, hit))
	{
		// Since intensity is per steradian, multiply by the entire sphere=4pi.
		Phi = intensity * M_PIf * 4;
		return true;
	}

	return false;
}

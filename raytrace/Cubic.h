#pragma once

#include <vector>

class Cubic
{
public:
	Cubic(float a, float b, float c, float d) noexcept
		: a(a), b(b), c(c), d(d)
	{ }

	inline float evaluate(float x) const noexcept
	{
		return d + x * (c + x * (b + x * a));
	}
	/// <summary>
	/// Finds the non-double roots of the cubic by use of either Cardano's formula or Vi�te's solution depending on the discriminant.
	/// </summary>
	/// <returns>A vector with either 1 or 3 roots.</returns>
	std::vector<float> find_roots() const;

private:
	/// <summary>
	/// Coefficients for the polynomial.
	/// P(x)=ax^3+bx^2+cx+d
	/// </summary>
	const float a, b, c, d;
};

// 02562 Rendering Framework
// Written by Jeppe Revall Frisvad, 2011
// Copyright (c) DTU Informatics 2011

#include <optix_world.h>
#include "HitInfo.h"
#include "PhotonCaustics.h"

using namespace optix;

/// <summary>
/// Finds the estimated radiance for a non-specular surface taking into account caustics.
/// </summary>
/// <param name="r">The ray that hit the material.</param>
/// <param name="hit">Information about the ray-surface intersection.</param>
/// <param name="emit">Used by Emission::shade.</param>
/// <returns>The estimated radiance at the intersection point.</returns>
float3 PhotonCaustics::shade(const Ray& r, HitInfo& hit, bool emit) const
{
	const float3 rho_d = get_diffuse(hit);

	// Finds the estimated caustics irradiance.
	const auto irradiance_estimate = tracer->caustics_irradiance(hit, max_dist, photons);
	// Multiply by the diffuse reflectance to get radiance.
	const auto caustics_component = irradiance_estimate * rho_d;

	// Calls the parent shader to find the Lambertian component of the radiance.
	const auto lambertian_component = Lambertian::shade(r, hit, emit);

	// Make a radiance estimate using the caustics photon map
	//
	// Input:  r          (the ray that hit the material)
	//         hit        (info about the ray-surface intersection)
	//         emit       (passed on to Emission::shade)
	//
	// Return: radiance reflected to where the ray was coming from
	//
	// Relevant data fields that are available (see PhotonCaustics.h and above):
	// rho_d              (difuse reflectance of the material)
	// tracer             (pointer to particle tracer)
	// max_dist           (maximum radius of radiance estimate)
	// photons            (maximum number of photons to be included in radiance estimate)
	//
	// Hint: Use the function tracer->caustics_irradiance(...) to do an
	//       irradiance estimate using the photon map. This is not the 
	//       same as a radiance estimate.

	return caustics_component + lambertian_component;
}

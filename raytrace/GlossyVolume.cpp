// 02562 Rendering Framework
// Written by Jeppe Revall Frisvad, 2011
// Copyright (c) DTU Informatics 2011

#include <optix_world.h>
#include "HitInfo.h"
#include "int_pow.h"
#include "GlossyVolume.h"
#include "Glossy.h"

using namespace optix;

#ifndef M_1_PIf
#define M_1_PIf 0.31830988618379067154
#endif

float3 GlossyVolume::shade(const Ray& r, HitInfo& hit, bool emit) const
{
	//Phong
	const float3 rho_d = get_diffuse(hit);
	const float3 rho_s = get_specular(hit);
	const float s = get_shininess(hit);

	auto radiance = make_float3(0);
	for (auto light : lights)
	{
		float3 dir_to_light;
		float3 light_radiance;
		if (light->sample(hit.position, dir_to_light, light_radiance))
		{
			const auto cos_light_normal = dot(dir_to_light, hit.shading_normal);
			if (cos_light_normal > 0)
			{
				const auto light_reflect_dir = optix::reflect(-dir_to_light, hit.shading_normal);
				const auto cos_obs_reflect = -dot(r.direction, light_reflect_dir);
				if (cos_obs_reflect > 0)
					radiance += (rho_s * (s * 0.5 + 1) * pow(cos_obs_reflect, s)) * light_radiance * cos_light_normal * M_1_PIf;
			}
		}
	}

	auto phong = radiance;

	//glossy shade
	if (hit.trace_depth >= max_depth)
		return make_float3(0.0f);

	float R;
	Ray reflected, refracted;
	HitInfo hit_reflected, hit_refracted;
	tracer->trace_reflected(r, hit, reflected, hit_reflected);
	tracer->trace_refracted(r, hit, refracted, hit_refracted, R);
	auto glossy_shade = R * (shade_new_ray(reflected, hit_reflected, emit) + phong) + (1.0f - R) * shade_new_ray(refracted, hit_refracted, emit);

   //volume
	if (1 < hit.ray_ior)
	{
		const auto T = get_transmittance(hit);
		const auto L0 = glossy_shade;
		return make_float3(L0.x * T.x, L0.y * T.y, L0.z * T.z);
	}
	else
		return glossy_shade;
	
	// Compute the specular part of the glossy shader and attenuate it
  // by the transmittance of the material if the ray is inside (as in
  // the volume shader).
}

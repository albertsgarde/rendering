// 02562 Rendering Framework
// Written by Jeppe Revall Frisvad, 2011
// Copyright (c) DTU Informatics 2011

#include <optix_world.h>
#include "HitInfo.h"
#include "ObjMaterial.h"
#include "fresnel.h"
#include "RayTracer.h"

using namespace optix;

#ifndef REFLECT_OFFSET
	#define REFLECT_OFFSET 1e-4
#endif

bool RayTracer::trace_reflected(const Ray& in, const HitInfo& in_hit, Ray& out, HitInfo& out_hit) const
{
	// Use the optix::reflect function to get the reflected direction.
	const auto out_dir = optix::reflect(in.direction, in_hit.shading_normal);
	out = Ray(in_hit.position, out_dir, 0, REFLECT_OFFSET);
	// Trace the reflected ray.
	trace_to_closest(out, out_hit);
	out_hit.ray_ior = 1;
	out_hit.trace_depth = in_hit.trace_depth + 1;
	return out_hit.has_hit;
}

bool RayTracer::trace_refracted(const Ray& in, const HitInfo& in_hit, Ray& out, HitInfo& out_hit) const
{
	auto normal = in_hit.shading_normal;
	// Get the index of refraction.
	out_hit.ray_ior = get_ior_out(in, in_hit, normal);
	// Follow formulas on slides.
	const auto ior_ratio = in_hit.ray_ior / out_hit.ray_ior;
	const auto omega_i_dot_normal = dot(-in.direction, normal);
	const auto cos_squared = 1 - ior_ratio * ior_ratio * (1 - (omega_i_dot_normal * omega_i_dot_normal));
	if (cos_squared < 0)
	{
		// Total internal refraction. Reflect ray.
		return trace_reflected(in, in_hit, out, out_hit);
	}
	const auto out_dir = ior_ratio * (omega_i_dot_normal * normal + in.direction) - normal*sqrt(cos_squared);
	out = Ray(in_hit.position, out_dir, 0, REFLECT_OFFSET);
	trace_to_closest(out, out_hit);
	out_hit.trace_depth = in_hit.trace_depth + 1;

	return out_hit.has_hit;
}

bool RayTracer::trace_refracted(const Ray& in, const HitInfo& in_hit, Ray& out, HitInfo& out_hit, float& R) const
{
  // Initialize the refracted ray and trace it.
  // Compute the Fresnel reflectance (see fresnel.h) and return it in R.
  //
  // Input:  in         (the ray to be refracted)
  //         in_hit     (info about the ray-surface intersection)
  //
  // Output: out        (the refracted ray)
  //         out_hit    (info about the refracted ray)
  //
  // Return: true if the refracted ray hit anything
  //
  // Hints: (a) There is a refract function available in the OptiX math library.
  //        (b) Set out_hit.ray_ior and out_hit.trace_depth.
  //        (c) Remember that the function must handle total internal reflection.

	auto normal = in_hit.shading_normal;
	out_hit.ray_ior = get_ior_out(in, in_hit, normal);
	const auto ior_ratio = in_hit.ray_ior / out_hit.ray_ior;
	const auto omega_i_dot_normal = dot(-in.direction, normal);
	const auto cos_tetha_i = omega_i_dot_normal; // The angle of incidence
	const auto cos_tetha_t_squared = 1 - ior_ratio * ior_ratio * (1 - (omega_i_dot_normal * omega_i_dot_normal));
	const auto cos_tetha_t = sqrt(cos_tetha_t_squared);
	if (cos_tetha_t_squared < 0)
	{
		// Total internal refraction. Reflect ray.
		R = 1;
		return trace_reflected(in, in_hit, out, out_hit);
	}
	const auto out_dir = ior_ratio * (omega_i_dot_normal * normal + in.direction) - normal * sqrt(cos_tetha_t_squared);
	out = Ray(in_hit.position, out_dir, 0, REFLECT_OFFSET);
	trace_to_closest(out, out_hit);
	out_hit.trace_depth = in_hit.trace_depth + 1;


  R = fresnel_R(cos_tetha_i,cos_tetha_t, in_hit.ray_ior, out_hit.ray_ior);
  return out_hit.has_hit;
}

/// <summary>
/// Finds the index of refraction of the material on the other side of the 
/// surface hit.
/// If the surface is that of an object, the result will be 1 if the ray is 
/// coming from inside and the ior of the object material otherwise. 
/// </summary>
/// <param name="in">The ray that hits the surface.</param>
/// <param name="in_hit">Information about the ray-surface intersection.</param>
/// <param name="normal">An output argument for the normal at the hit pointing 
/// in the same direction as the outgoing ray</param>
/// <returns>The index of refraction of the material on the other side of the 
/// surface hit.</returns>
float RayTracer::get_ior_out(const Ray& in, 
		const HitInfo& in_hit, float3& normal) const
{
	// Get shading normal for hit surface.
	normal = in_hit.shading_normal;
	// If coming from inside the object.
	if(dot(normal, in.direction) > 0.0)
	{
		// If coming from inside object, flip normal and out ior must be 1.0.
		normal = -normal;
		return 1.0f;
	}
	// Get material of hit object.
	const ObjMaterial* m = in_hit.material;
	// Return material ior if it exists. 1.0 otherwise.
	return m ? m->ior : 1.0f;
}

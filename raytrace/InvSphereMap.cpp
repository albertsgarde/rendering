#include <optix_world.h>
#include "InvSphereMap.h"

using namespace optix;

#ifndef M_1_PIf
#define M_1_PIf 0.31830988618379067154
#endif

void InvSphereMap::project_direction(const float3& d, float& u, float& v) const
{
	const auto theta = asin(d.y);
	const auto phi = atan2(d.x, d.z);
	u = theta * M_1_PIf;
	v = phi * M_1_PIf * 0.5f;
  // Implement an inverse sphere map here.
}
// 02562 Rendering Framework
// Written by Jeppe Revall Frisvad, 2011
// Copyright (c) DTU Informatics 2011

#include <optix_world.h>
#include "HitInfo.h"
#include "int_pow.h"
#include "Glossy.h"

using namespace optix;

#ifndef M_1_PIf
#define M_1_PIf 0.31830988618379067154
#endif

/// <summary>
/// Finds the radiance at the point of intersection.
/// Adds both a weighted reflected, refracted and Phong component.
/// </summary>
/// <param name="r">The ray hitting the glossy surface.</param>
/// <param name="hit">Information about the ray-surface intersection.</param>
/// <param name="emit">Used by Emission::shade.</param>
/// <returns>The radiance at the point of intersection.</returns>
float3 Glossy::shade(const Ray& r, HitInfo& hit, bool emit) const
{
	// If trace depth is too high, return 0 immediately.
	if (hit.trace_depth >= max_depth)
		return make_float3(0.0f);

	// Trace both a refracted and a reflected ray.
	float R;
	Ray reflected, refracted;
	HitInfo hit_reflected, hit_refracted;
	tracer->trace_reflected(r, hit, reflected, hit_reflected);
	tracer->trace_refracted(r, hit, refracted, hit_refracted, R);
	
	// Find the contribution from both rays and phong.
	const auto reflected_component = R * shade_new_ray(reflected, hit_reflected, emit);
	const auto refracted_component = (1.0f - R) * shade_new_ray(refracted, hit_refracted, emit);
	const auto phong_component = R * Phong::shade(r, hit, emit);

	return reflected_component + refracted_component + phong_component;
	// Implement glossy reflection here.
	//
	// Input:  r          (the ray that hit the material)
	//         hit        (info about the ray-surface intersection)
	//         emit       (passed on to Emission::shade)
	//
	// Return: radiance reflected to where the ray was coming from
	//
	// Relevant data fields that are available (see Mirror.h and HitInfo.h):
	// max_depth          (maximum trace depth)
	// tracer             (pointer to ray tracer)
	// hit.trace_depth    (number of surface interactions previously suffered by the ray)
	//
	// Hint: Use the function shade_new_ray(...) to pass a newly traced ray to
	//       the shader for the surface it hit.
}

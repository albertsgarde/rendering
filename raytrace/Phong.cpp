// 02562 Rendering Framework
// Written by Jeppe Revall Frisvad, 2011
// Copyright (c) DTU Informatics 2011

#include <optix_world.h>
#include "HitInfo.h"
#include "Phong.h"

using namespace optix;

#ifndef M_1_PIf
#define M_1_PIf 0.31830988618379067154
#endif

float3 Phong::shade(const Ray& r, HitInfo& hit, bool emit) const
{
	const float3 rho_d = get_diffuse(hit);
	const float3 rho_s = get_specular(hit);
	const float s = get_shininess(hit);

	auto radiance = make_float3(0);
	// Loop through all light sources in the scene.
	for (auto light : lights)
	{
		// Get light info.
		float3 dir_to_light;
		float3 light_radiance;
		if (light->sample(hit.position, dir_to_light, light_radiance))
		{
			// Follow formulas on slides.
			const auto cos_light_normal = dot(dir_to_light, hit.shading_normal);
			if (cos_light_normal > 0)
			{
				const auto light_reflect_dir = optix::reflect(-dir_to_light, hit.shading_normal);
				const auto cos_obs_reflect = -dot(r.direction, light_reflect_dir);
				if (cos_obs_reflect > 0)
					radiance += (rho_d + rho_s * (s * 0.5 + 1) * pow(cos_obs_reflect, s)) * light_radiance * cos_light_normal * M_1_PIf;
			}
		}
	}

	return radiance;
}
